/* El objeto Juego sera el encargado del control de todo el resto de los Objetos
existentes.
Le dara ordenes al Dibujante para que dibuje entidades en la pantalla. Cargara
el mapa, chequeara colisiones entre los objetos y actualizara sus movimientos
y ataques. */

var Juego = {
  // Aca se configura el tamaño del canvas del juego
  anchoCanvas: 961,
  altoCanvas: 577,
  jugador: Jugador,
  vidasInicial: Jugador.vidas,
  // Indica si el jugador ganó
  ganador: false,
  // Indica si el jugador perdió
  perdedor: false,
  // Indicador que se utiliza para validar si se debe aumentar la velocidad
  MaximaVelocidad: 0,
  // Indica si se pasaron las instrucciones
  ContInstruc: 0,
  // Indica si el juego esta pausado
  Pausado: false,
  obstaculosCarretera: [
    /*Aca se agregan los obstaculos visibles.*/
    new Obstaculo('imagenes/valla_horizontal.png', 70, 200, 30, 30, 1),
    new Obstaculo('imagenes/valla_horizontal.png', 90, 450, 30, 30, 1),
    new Obstaculo('imagenes/valla_vertical.png', 150, 380, 30, 30, 1),
    new Obstaculo('imagenes/valla_vertical.png', 400, 380, 30, 30, 1),
    new Obstaculo('imagenes/valla_vertical.png', 400, 410, 30, 30, 1),
    new Obstaculo('imagenes/bache.png', 550, 110, 30, 30, 1)

  ],
  /* Estos son los bordes con los que se puede chocar, por ejemplo, la vereda.
   Ya estan ubicados en sus lugares correspondientes. Ya aparecen en el mapa, ya
   que son invisibles. No tenes que preocuparte por ellos.*/
  bordes: [
    // // Bordes
    new Obstaculo('', 0, 5, 961, 18, 0),
    new Obstaculo('', 0, 559, 961, 18, 0),
    new Obstaculo('', 0, 5, 18, 572, 0),
    new Obstaculo('', 943, 5, 18, 572, 0),
    // Veredas
    new Obstaculo('', 18, 23, 51, 536, 2),
    new Obstaculo('', 69, 507, 690, 52, 2),
    new Obstaculo('', 587, 147, 173, 360, 2),
    new Obstaculo('', 346, 147, 241, 52, 2),
    new Obstaculo('', 196, 267, 263, 112, 2),
    new Obstaculo('', 196, 23, 83, 244, 2),
    new Obstaculo('', 279, 23, 664, 56, 2),
    new Obstaculo('', 887, 79, 56, 480, 2)
  ],
  // Enemigos
  enemigos: [
    new ZombieCaminante('imagenes/zombie1.png', 50, 200, 10, 10, 1, { desdeX: 50, hastaX: 200, desdeY: 200, hastaY: 300 }),
    new ZombieCaminante('imagenes/zombie2.png', 50, 200, 10, 10, 2, { desdeX: 50, hastaX: 600, desdeY: 400, hastaY: 500 }),
    new ZombieCaminante('imagenes/zombie3.png', 300, 200, 10, 10, 1, { desdeX: 300, hastaX: 600, desdeY: 200, hastaY: 300 }),
    new ZombieCaminante('imagenes/zombie4.png', 300, 50, 10, 10, 1, { desdeX: 300, hastaX: 900, desdeY: 50, hastaY: 150 }),
    new ZombieCaminante('imagenes/zombie1.png', 700, 100, 10, 10, 1, { desdeX: 700, hastaX: 900, desdeY: 100, hastaY: 200 }),
    new ZombieCaminante('imagenes/zombie2.png', 700, 200, 10, 10, 2, { desdeX: 700, hastaX: 900, desdeY: 200, hastaY: 300 }),
    new ZombieCaminante('imagenes/zombie3.png', 700, 300, 10, 10, 2, { desdeX: 700, hastaX: 900, desdeY: 300, hastaY: 400 }),
    new ZombieCaminante('imagenes/zombie4.png', 700, 400, 10, 10, 3, { desdeX: 700, hastaX: 900, desdeY: 400, hastaY: 450 }),
    new ZombieConductor('imagenes/tren_horizontal.png', 400, 324, 90, 30, 5, { desdeX: 0, hastaX: 861 }, 'H'),
    new ZombieConductor('imagenes/tren_vertical.png', 643, 0, 30, 90, 3, { desdeY: 0, hastaY: 477 }, 'V'),
    new ZombieConductor('imagenes/tren_vertical.png', 674, 0, 30, 90, 2, { desdeY: 0, hastaY: 477 }, 'V'),
    new ZombieAuto('imagenes/auto_verde_derecha.png', 120, 490, 30, 15, 3, { desdeX: 70, hastaX: 550 }, 'D'),
    new ZombieAuto('imagenes/auto_verde_abajo.png', 820, 490, 15, 30, 3, { desdeY: 80, hastaY: 530 }, 'U')
  ]
}

/* Se cargan los recursos de las imagenes, para tener un acceso mas rápido a las mismas*/
Juego.iniciarRecursos = function () {
  Resources.load([
    'imagenes/mapa.png',
    'imagenes/mensaje_gameover.png',
    'imagenes/Splash.png',
    'imagenes/bache.png',
    'imagenes/tren_horizontal.png',
    'imagenes/tren_vertical.png',
    'imagenes/valla_horizontal.png',
    'imagenes/valla_vertical.png',
    'imagenes/zombie1.png',
    'imagenes/zombie2.png',
    'imagenes/zombie3.png',
    'imagenes/zombie4.png',
    'imagenes/auto_rojo_abajo.png',
    'imagenes/auto_rojo_arriba.png',
    'imagenes/auto_rojo_derecha.png',
    'imagenes/auto_rojo_izquierda.png',
    'imagenes/auto_verde_abajo.png',
    'imagenes/auto_verde_derecha.png',
    'imagenes/auto_verde_izquierda.png',
    'imagenes/auto_verde_arriba.png',
    'imagenes/Bandera_Llegada.png',
    'imagenes/Bandera_Llegada_2.png',
    'imagenes/Mensaje1.png',
    'imagenes/Mensaje2.png'
  ]);
  Resources.onReady(this.comenzar.bind(Juego));
};

// Agrega los bordes de las veredas a los obstaculos de la carretera
Juego.obstaculos = function () {
  return this.obstaculosCarretera.concat(this.bordes);
};

Juego.comenzar = function () {
  // Inicializar el canvas del juego
  Dibujante.inicializarCanvas(this.anchoCanvas, this.altoCanvas);
  // Mostrar instrucciones
  Instrucciones();
};

Juego.buclePrincipal = function () {
  // Se valida si se gano o perdio el juego
  if (this.ganoJuego() || this.terminoJuego()) {
    Dibujante.borrarAreaDeJuego();
    this.dibujarFondo();
  }
  else {
    //Se valida si el juego está pausado.
    if (!this.Pausado) {
      // Con update se actualiza la logica del juego, tanto ataques como movimientos
      this.update();
      // Funcion que dibuja por cada fotograma a los objetos en pantalla.
      this.dibujar();
    }
    // Esto es una forma de llamar a la funcion Juego.buclePrincipal() repetidas veces
    window.requestAnimationFrame(this.buclePrincipal.bind(this));
  }
};

Juego.update = function () {
  this.calcularAtaques();
  this.moverEnemigos();
}
// Captura las teclas y si coincide con alguna de las flechas tiene que
// hacer que el jugador principal se mueva
Juego.capturarMovimiento = function (tecla) {
  //Se valida si el juego esta pausado.
  if (!this.Pausado) {
    var movX = 0;
    var movY = 0;
    var velocidad = this.jugador.velocidad;

    // El movimiento esta determinado por la velocidad del jugador
    if (tecla == 'izq') {
      movX = -velocidad;
    }
    if (tecla == 'arriba') {
      movY = -velocidad;
    }
    if (tecla == 'der') {
      movX = velocidad;
    }
    if (tecla == 'abajo') {
      movY = velocidad;
    }

    // Si se puede mover hacia esa posicion hay que hacer efectivo este movimiento
    if (this.chequearColisiones(movX + this.jugador.x, movY + this.jugador.y)) {
      /* Aca tiene que estar la logica para mover al jugador invocando alguno
      de sus metodos  */
      this.jugador.Mover(tecla, movX, movY);
    }
  }
};

Juego.dibujar = function () {
  // Borrar el fotograma actual
  Dibujante.borrarAreaDeJuego();
  //Se pinta la imagen de fondo segun el estado del juego
  this.dibujarFondo();
  //Se dibuja al jugador
  Dibujante.dibujarEntidad(Jugador);

  // Se recorren los obstaculos de la carretera pintandolos
  this.obstaculosCarretera.forEach(function (obstaculo) {
    Dibujante.dibujarEntidad(obstaculo);
  });

  // Se recorren los enemigos pintandolos
  this.enemigos.forEach(function (enemigo) {
    Dibujante.dibujarEntidad(enemigo);
  });

  // El dibujante dibuja las vidas del jugador
  var tamanio = this.anchoCanvas / this.vidasInicial;
  Dibujante.dibujarRectangulo('white', 0, 0, this.anchoCanvas, 8);
  for (var i = 0; i < this.jugador.vidas; i++) {
    var x = tamanio * i
    Dibujante.dibujarRectangulo('red', x, 0, tamanio, 8);
  }
  // Se agrega la linea final y las banderas de llegada
  Dibujante.dibujarRectangulo('green', 750, 520, 150, 8);
  Dibujante.dibujarImagen('imagenes/Bandera_Llegada.png', 740, 480, 50, 46);
  Dibujante.dibujarImagen('imagenes/Bandera_Llegada_2.png', 860, 480, 50, 46);
};



/* Recorre los enemigos haciendo que se muevan. */
Juego.moverEnemigos = function () {
  this.enemigos.forEach(function (enemigo) {
    enemigo.mover();
  });
};

/* Recorre los enemigos para ver cual esta colisionando con el jugador
Si colisiona empieza el ataque el zombie, si no, deja de atacar.*/
Juego.calcularAtaques = function () {
  this.enemigos.forEach(function (enemigo) {
    if (this.intersecan(enemigo, this.jugador, this.jugador.x, this.jugador.y)) {
      enemigo.comenzarAtaque(this.jugador);
    } else {
      enemigo.dejarDeAtacar();
    }
  }, this);
};

/* Aca se chequea si el jugador se peude mover a la posicion destino.
 Es decir, que no haya obstaculos que se interpongan. De ser asi, no podra moverse */
Juego.chequearColisiones = function (x, y) {
  var puedeMoverse = true
  this.obstaculos().forEach(function (obstaculo) {
    if (this.intersecan(obstaculo, this.jugador, x, y)) {
      obstaculo.Chocar(this.jugador);
      puedeMoverse = false
    }
  }, this)
  return puedeMoverse
};

/* Este metodo chequea si los elementos 1 y 2 si cruzan en x e y.
 x e y representan la coordenada a la cual se quiere mover el elemento2*/
Juego.intersecan = function (elemento1, elemento2, x, y) {
  var izquierda1 = elemento1.x
  var derecha1 = izquierda1 + elemento1.ancho
  var techo1 = elemento1.y
  var piso1 = techo1 + elemento1.alto
  var izquierda2 = x
  var derecha2 = izquierda2 + elemento2.ancho
  var techo2 = y
  var piso2 = y + elemento2.alto

  return ((piso1 >= techo2) && (techo1 <= piso2) &&
    (derecha1 >= izquierda2) && (izquierda1 <= derecha2))
};

Juego.dibujarFondo = function () {
  // Si se termino el juego hay que mostrar el mensaje de game over de fondo
  if (this.perdedor) {
    Dibujante.dibujarImagen('imagenes/mensaje_gameover.png', 0, 5, this.anchoCanvas, this.altoCanvas);
    document.getElementById('Pausar').style.display = 'none';
    document.getElementById('reiniciar').style.display = 'flex';
  }
  // Si se gano el juego hay que mostrar el mensaje de ganoJuego de fondo
  else if (this.ganador) {
    Dibujante.dibujarImagen('imagenes/Splash.png', 190, 113, 500, 203);
    document.getElementById('Pausar').style.display = 'none';
    document.getElementById('reiniciar').style.display = 'flex';
  } else {
    Dibujante.dibujarImagen('imagenes/mapa.png', 0, 5, this.anchoCanvas, this.altoCanvas);
  }
};

Juego.terminoJuego = function () {
  if (this.jugador.vidas <= 0) {
    this.perdedor = true;
  }
  return this.perdedor;
};

/* Se gana el juego si se sobre pasa cierto altura y */
Juego.ganoJuego = function () {
  if ((this.jugador.y + this.jugador.alto) > 530) {
    this.ganador = true;
  }
  return this.ganador;
};

Juego.iniciarRecursos();

// Activa las lecturas del teclado al presionar teclas
document.addEventListener('keydown', function (e) {
  // Si el contador es mayor a uno significa que se esta manteniendo presionada la flecha y se aumenta la velocidad.
  Juego.MaximaVelocidad++
  if (Juego.MaximaVelocidad > 1) {
    Juego.jugador.AumentarVelocidad();
  }
  var allowedKeys = {
    37: 'izq',
    38: 'arriba',
    39: 'der',
    40: 'abajo'
  };
  Juego.capturarMovimiento(allowedKeys[e.keyCode]);
});
document.addEventListener('keyup', function (e) {
  // Si se suelta la tecla que estaba siendo presionada se disminuye la velocidad.
  Juego.MaximaVelocidad = 0;
  Juego.jugador.DisminuirVelocidad();
});

// Se muestran las instrucciones del juego antes de comenzar
function Instrucciones() {
  switch (Juego.ContInstruc) {
    case 0:
      Juego.ContInstruc++;
      Dibujante.dibujarImagen('imagenes/Mensaje1.png', 0, 5, Juego.anchoCanvas, Juego.altoCanvas);
      document.getElementById('Siguiente').style.display = 'flex';
      break;
    case 1:
      Juego.ContInstruc++;
      Dibujante.borrarAreaDeJuego();
      Dibujante.dibujarImagen('imagenes/Mensaje2.png', 0, 5, Juego.anchoCanvas, Juego.altoCanvas);
      break;
    case 2:
      Juego.ContInstruc++;
      document.getElementById('Siguiente').style.display = 'none';
      document.getElementById('Pausar').style.display = 'flex';
      //Comienza el juego
      Juego.buclePrincipal();
      break;
  }
};

// Funcion para pausar el juego cuando el usuario lo desee
function Pausar() {
  document.getElementById('Pausar').style.display = 'none';
  document.getElementById('Continuar').style.display = 'flex';
  Juego.Pausado = true;
};

// Funcion para continuar el juego cuando el usuario lo desee
function Continuar() {
  document.getElementById('Pausar').style.display = 'flex';
  document.getElementById('Continuar').style.display = 'none';
  Juego.Pausado = false;
}