/* Este zombie hereda todos los mismos atributos de la clase Enemigo. Y se agrega el atributo de direccion 
También se modifica la forma de moverse y la cantidad de vidas que le quita al jugador.*/
var ZombieAuto = function (sprite, x, y, ancho, alto, velocidad, rangoMov, Direccion) {
    this.Direccion = Direccion;
    Enemigo.call(this, sprite, x, y, ancho, alto, velocidad, rangoMov);
}
ZombieAuto.prototype = Object.create(Enemigo.prototype);
ZombieAuto.prototype.constructor = ZombieAuto;

ZombieAuto.prototype.mover = function () {
    if (this.Direccion == 'D' || this.Direccion == 'I') {
        this.x += this.velocidad;
    } else {
        this.y += this.velocidad;
    }

    if ((this.x < this.rangoMov.desdeX) || (this.x > this.rangoMov.hastaX)) {
        switch (this.Direccion) {
            case 'D':
                this.Direccion = 'I';
                this.sprite = 'imagenes/auto_verde_izquierda.png';
                break;
            case 'I':
                this.Direccion = 'D';
                this.sprite = 'imagenes/auto_verde_derecha.png';
                break;
        }
        this.velocidad *= -1;
    }
    if ((this.y < this.rangoMov.desdeY) || (this.y > this.rangoMov.hastaY)) {
        switch (this.Direccion) {
            case 'U':
                this.Direccion = 'A';
                this.sprite = 'imagenes/auto_verde_arriba.png';
                break;
            case 'A':
                this.Direccion = 'U';
                this.sprite = 'imagenes/auto_verde_abajo.png';
                break;
        }
        this.velocidad *= -1;
    }
};

ZombieAuto.prototype.atacar = function (jugador) {
    jugador.perderVidas(2);
};