/* El objeto jugador es un objeto literal */
var Jugador = {
  /* el sprite contiene la ruta de la imagen*/
  sprite: 'imagenes/auto_rojo_abajo.png',
  x: 130,
  y: 160,
  ancho: 15,
  alto: 30,
  velocidad: 10,
  vidas: 5,
  //Se utiliza para modificar la posición del jugador y la imagen.
  Mover: function (P_Tecla, P_MovX, P_MovY) {
    this.x += P_MovX;
    this.y += P_MovY;
    this.CambiarSprite(P_Tecla);
  },
  //Se disminuye la vida del jugador cuando es atacado.
  perderVidas: function (potencia) {
    this.vidas -= potencia;
  },
  // Se aumenta la velocidad del jugador
  AumentarVelocidad: function () {
    this.velocidad = 15;
  },
  // Se disminute la velocidad del jugador
  DisminuirVelocidad: function () {
    this.velocidad = 10;
  },
  // Se cambia la imagen del jugador
  CambiarSprite: function (P_Tecla) {
    switch (P_Tecla) {
      case 'izq':
        this.ancho = 30;
        this.alto = 15;
        this.sprite = 'imagenes/auto_rojo_izquierda.png';
        break;
      case 'arriba':
        this.ancho = 15;
        this.alto = 30;
        this.sprite = 'imagenes/auto_rojo_arriba.png';
        break;
      case 'der':
        this.ancho = 30;
        this.alto = 15;
        this.sprite = 'imagenes/auto_rojo_derecha.png';
        break;
      case 'abajo':
        this.ancho = 15;
        this.alto = 30;
        this.sprite = 'imagenes/auto_rojo_abajo.png';
        break;
    }
  }
}
