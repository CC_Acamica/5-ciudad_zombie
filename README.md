**Quinto proyecto de la carrera Desarrollo Web Full Stack. El objetivo del proyecto está en la introducción al paradigma de programación orientada a objetos. El html, CSS, y los archivos resources.js y dibujante.js fueron brindados por el instituto.**

---

## Objetivo del proyecto
En este proyecto vas a crear un juego utilizando el paradigma de programación orientada a objetos. En el juego, nuestro héroe, el jugador principal, tiene que llegar a la meta esquivando diferentes obstáculos y evitando ser atrapado por los zombies o el tren descontrolado. Este juego funciona online y vas a poder mostrar y compartir tu creación con quien desees.

Nuestro foco estará en poder captar la esencia de cada objeto y cuáles son sus responsabilidades. Estas deberán ser pensadas detenidamente, ya que no es correcto mezclar funcionalidades dispares en un mismo objeto. El objeto ‘zombie’ sabe como “atacar” pero sería incorrecto que además sepa cómo mostrar un botón de alerta en la pantalla. Para esto vas a aprender el paradigma de programación orientada a objetos en JavaScript y a continuar utilizando HTML.

---

## Proyecto finalizado
El proyecto finalizado se encuentra en el directorio ChristianCabrer-CiudadZombie.
Se puede ejecutar el mismo abriendo el archivo juego.html.

---

## Descripción de como se llevo a cabo el proyecto
* Primero comencé analizando todos los archivos que ya venian en los recursos para entender el funcionamiento del canvas e imagenes.
* Luego agregue todos los objetos que quería que aparecieran en la pantalla(jugador, obstáculos, enemigos).
* Después cree todas las clases que iba a necesitar para el juego: Jugador, Enemigo, Obstaculo, Zombie_auto, Zombie_caminante, Zombie_conductor.
* Complete todos los métodos de las clases creadas para darle poder darle vida a los objetos.
* Luego sume 2 métodos al jugador para aumentar y disminuir la velocidad. Si el jugador mantiene apretado el botón de las flechas para moverse la velocidad del jugador se incrementa en 15px, pero si se mueve de a 1 posición solo se mueve de a 10px.(ésto lo logré con el evento keyup).
* Y finalmente agregue la funcionalidad que permite pausar el juego, decidí agregar esa funcionalidad con un botón debajo del juego para poder pausar y continuar.

---